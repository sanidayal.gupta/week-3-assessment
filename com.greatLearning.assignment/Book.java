package com.greatLearning.assignment;

public class Book {

	private String name;
	private int price;
	private String genre;
	private int noOfCopyesSold;
	
	public Book(String name, int price, String genre, int noOfCopyesSold) {
		super();
		this.name = name;
		this.price = price;
		this.genre = genre;
		this.noOfCopyesSold = noOfCopyesSold;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getNoOfCopyesSold() {
		return noOfCopyesSold;
	}

	public void setNoOfCopyesSold(int noOfCopyesSold) {
		this.noOfCopyesSold = noOfCopyesSold;
	}

	@Override
	public String toString() {
		return "Book [name=" + name + ", price=" + price + ", genre=" + genre + ", noOfCopyesSold=" + noOfCopyesSold
				+ "]";
	}
	
	
	
}

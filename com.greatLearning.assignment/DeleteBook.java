package com.greatLearning.assignment;

import java.util.HashMap;
import java.util.Scanner;

public class DeleteBook {
	
	public static void delBook(HashMap<Integer,Book> book) throws InvalidInputException {
		if(book.size()==0) {
			System.out.println("!!!!! Book Store is Empty !! Firstly Add The Book.. !!");
		}else {
			Scanner sc=new Scanner(System.in);
			System.out.println("************** Deleting.. Books ***********");
			System.out.println("Enter the Book Id Which you want to delete");
			int id=sc.nextInt();
			
			if(id<0) {
				throw new InvalidInputException("Book Id Should be Greater than 0 ");
			}else {
				if(book.containsKey(id)) {
					Book b=book.get(id);
					System.out.println("------------ Book Details -----------");
					System.out.println("Book Id : "+id+"\nName : "+b.getName()+"\nPrice : "+b.getPrice()+"\nGeneration : "+b.getGenre()+"\nNo of Copies Sold : "+b.getNoOfCopyesSold());
					System.out.println("------------ ************ -----------");
					
					System.out.println("Are You Sure ? y/n : ");
					String choice=sc.next();
					String conChoice=choice.toLowerCase();
					
					if(conChoice.equals("y")) {
						book.remove(id);
						System.out.println("Book Deleted Successfully");
					}else if(conChoice.equals("n")) {	
						delBook(book);
					}
				}else {
					System.out.println("!! No Such Record Found !!");
				}
			}	
		}	
	}

}

package com.greatLearning.assignment;

import java.util.HashMap;
import java.util.Map;

public class DisplayBook {
	
	public static void dispBook(HashMap<Integer,Book> book) {
		
		if(book.size()==0) {
			System.out.println("!!!!! Book Store is Empty !! Firstly Add The Book.. !!");
		}else {
			
			System.out.println("---------------------------------------------------------------------------------");
			System.out.println("Book Id"+" | "+"Name"+" | "+"Price"+" | "+"Category"+" | "+"No of copies Sold");
			System.out.println("---------------------------------------------------------------------------------");
			
			for(Map.Entry m : book.entrySet()) {
		    	Book book1= (Book) m.getValue();
		    	System.out.println(m.getKey()+"  |  "+book1.getName()+"  |  "+book1.getPrice()+"  |  "+book1.getGenre()+"  |  "+book1.getNoOfCopyesSold());
		    }
			
		}		
		
	}

}

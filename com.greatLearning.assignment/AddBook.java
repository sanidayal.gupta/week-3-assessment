package com.greatLearning.assignment;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class AddBook {
	
	public static void addBook(HashMap<Integer,Book> book) throws InvalidInputException {
		Scanner sc=new Scanner(System.in);
		Random random=new Random();
		System.out.println("************** Addinng.. Books ***********");
		System.out.println("How Many Books you want to add : ");
		int count=sc.nextInt();
		for(int i=1;i<=count;i++) {
			int id=random.nextInt(50);
		
			System.out.print("Enter book Name : ");
			String name=sc.next();
			if(name.equals(null)) {
				throw new InvalidInputException("Please enter proper Name");
			}else {
				System.out.print("Enter book Price in INR : ");
				int price=sc.nextInt();
				
				if(price<0) {
					throw new InvalidInputException("Price Should be Greater Than 0");
				}else {
					System.out.print("Enter book Genre : ");
					String genre=sc.next();
					
					if(genre.equals(null)) {
						throw new InvalidInputException("Please enter proper Category");
					}else {
						System.out.print("Enter No. of Copies Sold : ");
						int noOfCopiesSold=sc.nextInt();
						
						if(noOfCopiesSold<0) {
							throw new InvalidInputException("Value Should be greater than 0");
						}else {
							book.put(id, new Book(name, price, genre, noOfCopiesSold));
						}
					}
				}
			}
			System.out.println("\n");
			System.out.println("Book "+i+" is added Successfully");
			System.out.println("\n");
		}
	}
}
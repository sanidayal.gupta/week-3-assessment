package com.greatLearning.assignment;

import java.util.HashMap;
import java.util.Scanner;

public class MagicOfBooks {

	public static void main(String[] args) throws InvalidInputException {
		// TODO Auto-generated method stub
		HashMap<Integer,Book> book=new HashMap<>();
		 boolean ASC = true;
		 boolean DESC = false;
		 Scanner sc=new Scanner(System.in);
		 
		line:  while(true) {
				System.out.println("-------------------------");
			 System.out.println("1.Add a Book\n2.Delete a Book\n3.Update a Book\n4.Display all Book\n5.Book Count\n6.Autobiography Books\n7.Price Low to High\n8.Price High to Low\n9.Best Selling\n10.Exit");
			 System.out.println(" \nMake Your choice : ");
			 int choice =sc.nextInt();
			 System.out.println("-------------------------");
			 switch(choice) {
			 	case 1:AddBook.addBook(book);
			 			break;
			 	case 2:DeleteBook.delBook(book);
		 				break;
			 	case 3:UpdateBook.updateBook(book);
		 				break;
			 	case 4:DisplayBook.dispBook(book);
		 				break;
			 	case 5:BookCount.countBook(book);
		 				break;
			 	case 6: SearchingAndSorting.autobiographySearch(book);
			 			break;
			 	case 7:	SearchingAndSorting.sortByPrice(book, ASC);
			 			break;
			 	case 8:	SearchingAndSorting.sortByPrice(book, DESC);
			 			break;
			 	case 9: SearchingAndSorting.bestSelling(book);
			 			break;
			 	case 10:break line;		
			 	default:System.out.println("Invalid Choice !!! Please Make Correct choice !");		

			 }
		 }						
	}

}

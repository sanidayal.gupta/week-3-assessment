package com.greatLearning.assignment;

import java.util.HashMap;
import java.util.Scanner;

public class UpdateBook {
	
	public static void updateBook(HashMap<Integer,Book> book) throws InvalidInputException {
		if(book.size()==0) {
			System.out.println("!!!!! Book Store is Empty !! Firstly Add The Book.. !!");
		}else {
			Scanner sc=new Scanner(System.in);
			System.out.println("************** Updating.. Books ***********");
			System.out.println("Enter the Book Id Which you want to Update");
			int id=sc.nextInt();
			
			if(id<0) {
				throw new InvalidInputException("Book Id Should be Greater than 0 ");
			}else {
				if(book.containsKey(id)) {
					Book b=book.get(id);
					System.out.println("------------ Book Details -----------");
					System.out.println("Book Id : "+id+"\nName : "+b.getName()+"\nPrice : "+b.getPrice()+"\nGeneration : "+b.getGenre()+"\nNo of Copies Sold : "+b.getNoOfCopyesSold());
					System.out.println("------------ ************ -----------\n");
					System.out.println("------------ Update Book Details -----------\n");
					
					System.out.print("Enter book Name : ");
					String name=sc.next();
					
					System.out.print("Enter book Price in INR : ");
					int price=sc.nextInt();
				
					System.out.print("Enter book Genre : ");
					String genre=sc.next();
					
					System.out.print("Enter No. of Copies Sold : ");
					int noOfCopiesSold=sc.nextInt();
					
					book.replace(id, new Book(name, price, genre, noOfCopiesSold));
					System.out.println("------------ Updated Successfully -----------\n");
				}else {
					System.out.println("!! No Such Record Found !!");
				}
			}
		}
		}	

}

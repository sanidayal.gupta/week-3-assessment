package com.greatLearning.assignment;

public class InvalidInputException extends Exception {
	
	String errorMsg;

	public InvalidInputException(String errorMsg) {
		super();
		this.errorMsg = errorMsg;
	}

	@Override
	public String toString() {
		return "InvalidInputException [Error=" + errorMsg + "]";
	}
	

}

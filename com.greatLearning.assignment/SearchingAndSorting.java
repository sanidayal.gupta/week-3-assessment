package com.greatLearning.assignment;

import java.util.Map.Entry;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SearchingAndSorting {
	
	public static void autobiographySearch(HashMap<Integer,Book> book) {
		if(book.size()==0) {
			System.out.println("!!!!! Book Store is Empty !! Firstly Add The Book.. !!");
		}else {
			System.out.println("********* Autobiography Category Books : **********");
			System.out.println("----------------------");
			System.out.println("Book Id"+" | "+"Name");
			System.out.println("----------------------");
			
			for(Map.Entry m : book.entrySet()) {
				Book b=(Book) m.getValue();
				String cat = b.getGenre().toLowerCase();
				if(cat.equals("autobiography")) {
					System.out.println(m.getKey()+"  |  "+b.getName());
				}
			}
		}
	}
	
	public static void sortByPrice(HashMap<Integer,Book> book,boolean order) {
		
		if(book.size()==0) {
			System.out.println("!!!!! Book Store is Empty !! Firstly Add The Book.. !!");
		}else {
			List<Entry<Integer,Book>> list = new LinkedList<Entry<Integer,Book>>(book.entrySet());
			
			//sorting the list elements  
			Collections.sort(list, new Comparator<Entry<Integer,Book>>()   
			{  
			public int compare(Entry<Integer,Book> o1, Entry<Integer,Book> o2)   
			{  
				Integer p1=o1.getValue().getPrice();
				Integer p2=o2.getValue().getPrice();
			if (order)   
			{  
				System.out.println("!!!!! Low Price to High !!!!!!");
				return p1.compareTo(p2);}   
			else   
			{  
				System.out.println("!!!!! High Price to Low !!!!!!");
				return p2.compareTo(p1);  
			}  
			}  
			});  
			//prints the sorted HashMap  
			HashMap<Integer,Book> sortedMap = new LinkedHashMap<Integer,Book>();  
			for (Entry<Integer,Book> entry : list)   
			{  
			sortedMap.put(entry.getKey(), entry.getValue());  
			}  
			DisplayBook.dispBook(sortedMap);
		}
	}
	
	//Best Selling
	public static void bestSelling(HashMap<Integer,Book> book) {
		if(book.size()==0) {
			System.out.println("!!!!! Book Store is Empty !! Firstly Add The Book.. !!");
		}else {
			System.out.println("!!!!! Best Selling Books !!!!!");
			List<Entry<Integer,Book>> list = new LinkedList<Entry<Integer,Book>>(book.entrySet());
			
			//sorting the list elements  
			Collections.sort(list, new Comparator<Entry<Integer,Book>>()   
			{  
			public int compare(Entry<Integer,Book> o1, Entry<Integer,Book> o2)   
			{  
				Integer copy1=o1.getValue().getNoOfCopyesSold();
				Integer copy2=o2.getValue().getNoOfCopyesSold();
				return copy2.compareTo(copy1);
			}
			});
			//prints the sorted HashMap  
			HashMap<Integer,Book> sortedMap = new LinkedHashMap<Integer,Book>();  
			for (Entry<Integer,Book> entry : list)   
			{  
			sortedMap.put(entry.getKey(), entry.getValue());  
			}  
			DisplayBook.dispBook(sortedMap);
		}
	}
}
